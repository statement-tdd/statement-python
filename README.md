# statement-python

## Setup

### How to setup virtual environment

*nix
```shell 
virtualenv venv
```
or
```shell
python -m venv venv
```

### How to activate venv & install deps  
```
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
``` 

### How to run tests
```shell
python -m unittest statement_test.py
```

### Docker

```shell
docker build -t statement-python .
docker run -v "$(pwd):/usr/src/app" -it statement-python /bin/bash
```


